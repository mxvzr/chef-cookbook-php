define php_fpm_pool :name, :enable => true do 
    case node["plateform_family"]
    when "amazon", "fedora"
        service_name = 'php-fpm'
    else
        service_name = 'php5-fpm'
    end
        
    if params[:enable]
        vars = {
            "name" => params['name'],
            #"user" => params.key?('user') ? (params['user'] : node['php-fpm']['user']),
            #"group" => params.key?('group') ? (params['group'] : node['php-fpm']['group']),
            "listen" => params['listen'],
            "pm" => params.key?('pm') ? (params['pm'] : 'static'),
            "pm_max_children" => params["pm_max_children"]
        }
        
        if (params.key?("prefix")) vars["prefix"] = params["prefix"];
        if (params.key?("listen_backlog")) vars["listen_backlog"] = params["listen_backlog"];
        if (params.key?("listen_allowed_clients")) vars["listen_allowed_clients"] = params["listen_allowed_clients"];
        if (params.key?("listen_owner")) vars["listen_owner"] = params["listen_owner"];
        if (params.key?("listen_group")) vars["listen_group"] = params["listen_group"];
        if (params.key?("listen_mode")) vars["listen_mode"] = params["listen_mode"];
        if (params.key?("pm_min_spare_servers")) vars["pm_min_spare_servers"] = params["pm_min_spare_servers"];
        if (params.key?("pm_max_spare_servers")) vars["pm_max_spare_servers"] = params["pm_max_spare_servers"];
        if (params.key?("pm_start_servers")) vars["pm_start_servers"] = params["pm_start_servers"];
        if (params.key?("pm_process_idle_timeout")) vars["pm_process_idle_timeout"] = params["pm_process_idle_timeout"];
        if (params.key?("pm_max_requests")) vars["pm_max_requests"] = params["pm_max_requests"];
        if (params.key?("pm_status_path")) vars["pm_status_path"] = params["pm_status_path"];
        if (params.key?("ping_path")) vars["ping_path"] = params["ping_path"];
        if (params.key?("ping_response")) vars["ping_response"] = params["ping_response"];
        if (params.key?("access_log")) vars["access_log"] = params["access_log"];
        if (params.key?("access_format")) vars["access_format"] = params["access_format"];
        if (params.key?("request_terminate_timeout")) vars["request_terminate_timeout"] = params["request_terminate_timeout"];
        if (params.key?("request_slowlog_timeout")) vars["request_slowlog_timeout"] = params["request_slowlog_timeout"];
        if (params.key?("slowlog")) vars["slowlog"] = params["slowlog"];
        if (params.key?("rlimit_files")) vars["rlimit_files"] = params["rlimit_files"];
        if (params.key?("rlimit_core")) vars["rlimit_core"] = params["rlimit_core"];
        if (params.key?("chdir")) vars["chdir"] = params["chdir"];
        if (params.key?("catch_workers_output")) vars["catch_workers_output"] = params["catch_workers_output"];
        if (params.key?("security_limit_extensions")) vars["security_limit_extensions"] = params["security_limit_extensions"];
        if (params.key?("env")) vars["env"] = params["env"];
        if (params.key?("php_admin_value")) vars["php_admin_value"] = params["php_admin_value"];
        if (params.key?("php_admin_flag")) vars["php_admin_flag"] = params["php_admin_flag"];
        if (params.key?("php_flag")) vars["php_flag"] = params["php_flag"];
        if (params.key?("php_value")) vars["php_value"] = params["php_value"];
        
        template "#{node[:php][:ext_conf_dir]}/fpm/conf.d/#{params[:name]}.conf" do
            source "fpm-pool.conf.erb"
            variables(vars)
            notifies :reload, resources(:service => service_name), :delayed
        end
    else
        file "#{node[:php-fpm][:ext_conf_dir]}/fpm/conf.d/#{params[:name]}.conf" do
            action :delete
            notifies :reload, resources(:service => service_name), :delayed
        end
    end
end